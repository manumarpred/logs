from utils import Logger


class DoSomething:

    logger_file = Logger.get_logger_file('DoSomethingFile')
    logger_console = Logger.get_logger_console('DoSomethingConsole')

    @classmethod
    def do_something(cls):

        # primer log
        print('lógica complicada')
        cls.logger_file.info('guardando en file')
        cls.logger_console.info('mostrando en consola')

        # segundo log
        suma = 1 + 1
        cls.logger_file.info(f'la suma es {suma}')

        # tercer log lanzando una excepcion
        try:
            raise Exception('Error')
        except Exception as e:
            cls.logger_file.exception(f'Excepcion lanzada intencionalmente: {e}')
            cls.logger_console.critical(f'Error: {e}')


if __name__ == '__main__':
    DoSomething.do_something()