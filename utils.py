import logging
import os


class Logger:

    @staticmethod
    def get_logger_file(name):
        # Create a custom logger
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)

        # Create handlers
        c_handler = logging.FileHandler('log/logs.txt')
       
        # Create formatters and add it to handlers
        format_c = logging.Formatter(
                                    '%(levelname)s - '
                                    '%(pathname)s - '
                                    '%(message)s -'
                                    '%(lineno)d -'
                                    '%(asctime)s'
                                    )
       
        c_handler.setFormatter(format_c)
        logger.addHandler(c_handler)
        # Add handlers to the logger
        return logger

    @staticmethod
    def get_logger_console(name):
        # Create a custom logger
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)

        # Create handlers
        c_handler = logging.StreamHandler()
       
        # Create formatters and add it to handlers
        format_c = logging.Formatter(
                                    '%(levelname)s - '
                                    '%(pathname)s - '
                                    '%(message)s -'
                                    '%(lineno)d -'
                                    '%(asctime)s'
                                    )
       
        c_handler.setFormatter(format_c)
        logger.addHandler(c_handler)
        # Add handlers to the logger
        return logger